(function() {
    'use strict';

    angular
        .module('datatransferApp')
        .controller('DataTransferJobHistoryDialogController', DataTransferJobHistoryDialogController);

    DataTransferJobHistoryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DataTransferJobHistory'];

    function DataTransferJobHistoryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DataTransferJobHistory) {
        var vm = this;

        vm.dataTransferJobHistory = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.dataTransferJobHistory.id !== null) {
                DataTransferJobHistory.update(vm.dataTransferJobHistory, onSaveSuccess, onSaveError);
            } else {
                DataTransferJobHistory.save(vm.dataTransferJobHistory, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('datatransferApp:dataTransferJobHistoryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.executionTime = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
