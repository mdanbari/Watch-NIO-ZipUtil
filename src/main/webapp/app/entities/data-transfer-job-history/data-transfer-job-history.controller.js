(function() {
    'use strict';

    angular
        .module('datatransferApp')
        .controller('DataTransferJobHistoryController', DataTransferJobHistoryController);

    DataTransferJobHistoryController.$inject = ['DataTransferJobHistory'];

    function DataTransferJobHistoryController(DataTransferJobHistory) {

        var vm = this;

        vm.dataTransferJobHistories = [];

        loadAll();

        function loadAll() {
            DataTransferJobHistory.query(function(result) {
                vm.dataTransferJobHistories = result;
                vm.searchQuery = null;
            });
        }
    }
})();
