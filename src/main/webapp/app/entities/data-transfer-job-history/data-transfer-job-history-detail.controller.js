(function() {
    'use strict';

    angular
        .module('datatransferApp')
        .controller('DataTransferJobHistoryDetailController', DataTransferJobHistoryDetailController);

    DataTransferJobHistoryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DataTransferJobHistory'];

    function DataTransferJobHistoryDetailController($scope, $rootScope, $stateParams, previousState, entity, DataTransferJobHistory) {
        var vm = this;

        vm.dataTransferJobHistory = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('datatransferApp:dataTransferJobHistoryUpdate', function(event, result) {
            vm.dataTransferJobHistory = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
