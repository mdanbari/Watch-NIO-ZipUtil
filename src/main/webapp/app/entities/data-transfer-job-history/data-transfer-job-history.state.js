(function() {
    'use strict';

    angular
        .module('datatransferApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('data-transfer-job-history', {
            parent: 'entity',
            url: '/data-transfer-job-history',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'DataTransferJobHistories'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/data-transfer-job-history/data-transfer-job-histories.html',
                    controller: 'DataTransferJobHistoryController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('data-transfer-job-history-detail', {
            parent: 'data-transfer-job-history',
            url: '/data-transfer-job-history/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'DataTransferJobHistory'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/data-transfer-job-history/data-transfer-job-history-detail.html',
                    controller: 'DataTransferJobHistoryDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'DataTransferJobHistory', function($stateParams, DataTransferJobHistory) {
                    return DataTransferJobHistory.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'data-transfer-job-history',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('data-transfer-job-history-detail.edit', {
            parent: 'data-transfer-job-history-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/data-transfer-job-history/data-transfer-job-history-dialog.html',
                    controller: 'DataTransferJobHistoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DataTransferJobHistory', function(DataTransferJobHistory) {
                            return DataTransferJobHistory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('data-transfer-job-history.new', {
            parent: 'data-transfer-job-history',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/data-transfer-job-history/data-transfer-job-history-dialog.html',
                    controller: 'DataTransferJobHistoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                executionTime: null,
                                fileName: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('data-transfer-job-history', null, { reload: 'data-transfer-job-history' });
                }, function() {
                    $state.go('data-transfer-job-history');
                });
            }]
        })
        .state('data-transfer-job-history.edit', {
            parent: 'data-transfer-job-history',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/data-transfer-job-history/data-transfer-job-history-dialog.html',
                    controller: 'DataTransferJobHistoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DataTransferJobHistory', function(DataTransferJobHistory) {
                            return DataTransferJobHistory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('data-transfer-job-history', null, { reload: 'data-transfer-job-history' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('data-transfer-job-history.delete', {
            parent: 'data-transfer-job-history',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/data-transfer-job-history/data-transfer-job-history-delete-dialog.html',
                    controller: 'DataTransferJobHistoryDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DataTransferJobHistory', function(DataTransferJobHistory) {
                            return DataTransferJobHistory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('data-transfer-job-history', null, { reload: 'data-transfer-job-history' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
