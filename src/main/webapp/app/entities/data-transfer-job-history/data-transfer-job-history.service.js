(function() {
    'use strict';
    angular
        .module('datatransferApp')
        .factory('DataTransferJobHistory', DataTransferJobHistory);

    DataTransferJobHistory.$inject = ['$resource', 'DateUtils'];

    function DataTransferJobHistory ($resource, DateUtils) {
        var resourceUrl =  'api/data-transfer-job-histories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.executionTime = DateUtils.convertDateTimeFromServer(data.executionTime);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
