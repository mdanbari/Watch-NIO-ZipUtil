(function() {
    'use strict';

    angular
        .module('datatransferApp')
        .controller('DataTransferJobHistoryDeleteController',DataTransferJobHistoryDeleteController);

    DataTransferJobHistoryDeleteController.$inject = ['$uibModalInstance', 'entity', 'DataTransferJobHistory'];

    function DataTransferJobHistoryDeleteController($uibModalInstance, entity, DataTransferJobHistory) {
        var vm = this;

        vm.dataTransferJobHistory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DataTransferJobHistory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
