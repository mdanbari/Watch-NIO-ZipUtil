(function() {
    'use strict';

    angular
        .module('datatransferApp')
        .controller('DataTransferConfigDetailController', DataTransferConfigDetailController);

    DataTransferConfigDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DataTransferConfig'];

    function DataTransferConfigDetailController($scope, $rootScope, $stateParams, previousState, entity, DataTransferConfig) {
        var vm = this;

        vm.dataTransferConfig = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('datatransferApp:dataTransferConfigUpdate', function(event, result) {
            vm.dataTransferConfig = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
