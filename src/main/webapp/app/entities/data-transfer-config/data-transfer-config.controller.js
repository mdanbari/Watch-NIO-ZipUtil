(function() {
    'use strict';

    angular
        .module('datatransferApp')
        .controller('DataTransferConfigController', DataTransferConfigController);

    DataTransferConfigController.$inject = ['DataTransferConfig'];

    function DataTransferConfigController(DataTransferConfig) {

        var vm = this;

        vm.dataTransferConfigs = [];

        loadAll();

        function loadAll() {
            DataTransferConfig.query(function(result) {
                vm.dataTransferConfigs = result;
                vm.searchQuery = null;
            });
        }
    }
})();
