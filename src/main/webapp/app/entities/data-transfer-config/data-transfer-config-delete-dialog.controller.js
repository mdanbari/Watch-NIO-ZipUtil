(function() {
    'use strict';

    angular
        .module('datatransferApp')
        .controller('DataTransferConfigDeleteController',DataTransferConfigDeleteController);

    DataTransferConfigDeleteController.$inject = ['$uibModalInstance', 'entity', 'DataTransferConfig'];

    function DataTransferConfigDeleteController($uibModalInstance, entity, DataTransferConfig) {
        var vm = this;

        vm.dataTransferConfig = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DataTransferConfig.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
