(function() {
    'use strict';

    angular
        .module('datatransferApp')
        .controller('DataTransferConfigDialogController', DataTransferConfigDialogController);

    DataTransferConfigDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DataTransferConfig'];

    function DataTransferConfigDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DataTransferConfig) {
        var vm = this;

        vm.dataTransferConfig = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.dataTransferConfig.id !== null) {
                DataTransferConfig.update(vm.dataTransferConfig, onSaveSuccess, onSaveError);
            } else {
                DataTransferConfig.save(vm.dataTransferConfig, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('datatransferApp:dataTransferConfigUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
