(function() {
    'use strict';

    angular
        .module('datatransferApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('data-transfer-config', {
            parent: 'entity',
            url: '/data-transfer-config',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'DataTransferConfigs'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/data-transfer-config/data-transfer-configs.html',
                    controller: 'DataTransferConfigController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('data-transfer-config-detail', {
            parent: 'data-transfer-config',
            url: '/data-transfer-config/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'DataTransferConfig'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/data-transfer-config/data-transfer-config-detail.html',
                    controller: 'DataTransferConfigDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'DataTransferConfig', function($stateParams, DataTransferConfig) {
                    return DataTransferConfig.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'data-transfer-config',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('data-transfer-config-detail.edit', {
            parent: 'data-transfer-config-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/data-transfer-config/data-transfer-config-dialog.html',
                    controller: 'DataTransferConfigDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DataTransferConfig', function(DataTransferConfig) {
                            return DataTransferConfig.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('data-transfer-config.new', {
            parent: 'data-transfer-config',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/data-transfer-config/data-transfer-config-dialog.html',
                    controller: 'DataTransferConfigDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                sourceFolderPath: null,
                                remoteType: null,
                                remoteAddress: null,
                                remoteUsername: null,
                                remotePassword: null,
                                jobIntervalsec: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('data-transfer-config', null, { reload: 'data-transfer-config' });
                }, function() {
                    $state.go('data-transfer-config');
                });
            }]
        })
        .state('data-transfer-config.edit', {
            parent: 'data-transfer-config',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/data-transfer-config/data-transfer-config-dialog.html',
                    controller: 'DataTransferConfigDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DataTransferConfig', function(DataTransferConfig) {
                            return DataTransferConfig.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('data-transfer-config', null, { reload: 'data-transfer-config' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('data-transfer-config.delete', {
            parent: 'data-transfer-config',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/data-transfer-config/data-transfer-config-delete-dialog.html',
                    controller: 'DataTransferConfigDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DataTransferConfig', function(DataTransferConfig) {
                            return DataTransferConfig.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('data-transfer-config', null, { reload: 'data-transfer-config' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
