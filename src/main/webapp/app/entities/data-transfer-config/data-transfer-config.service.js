(function() {
    'use strict';
    angular
        .module('datatransferApp')
        .factory('DataTransferConfig', DataTransferConfig);

    DataTransferConfig.$inject = ['$resource'];

    function DataTransferConfig ($resource) {
        var resourceUrl =  'api/data-transfer-configs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
