(function() {
    'use strict';

    angular
        .module('datatransferApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
