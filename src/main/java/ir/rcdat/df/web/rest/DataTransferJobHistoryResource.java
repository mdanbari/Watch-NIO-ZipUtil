package ir.rcdat.df.web.rest;

import com.codahale.metrics.annotation.Timed;
import ir.rcdat.df.domain.DataTransferJobHistory;
import ir.rcdat.df.service.DataTransferJobHistoryService;
import ir.rcdat.df.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DataTransferJobHistory.
 */
@RestController
@RequestMapping("/api")
public class DataTransferJobHistoryResource {

    private final Logger log = LoggerFactory.getLogger(DataTransferJobHistoryResource.class);

    private static final String ENTITY_NAME = "dataTransferJobHistory";

    private final DataTransferJobHistoryService dataTransferJobHistoryService;

    public DataTransferJobHistoryResource(DataTransferJobHistoryService dataTransferJobHistoryService) {
        this.dataTransferJobHistoryService = dataTransferJobHistoryService;
    }

    /**
     * POST  /data-transfer-job-histories : Create a new dataTransferJobHistory.
     *
     * @param dataTransferJobHistory the dataTransferJobHistory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dataTransferJobHistory, or with status 400 (Bad Request) if the dataTransferJobHistory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/data-transfer-job-histories")
    @Timed
    public ResponseEntity<DataTransferJobHistory> createDataTransferJobHistory(@Valid @RequestBody DataTransferJobHistory dataTransferJobHistory) throws URISyntaxException {
        log.debug("REST request to save DataTransferJobHistory : {}", dataTransferJobHistory);
        if (dataTransferJobHistory.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dataTransferJobHistory cannot already have an ID")).body(null);
        }
        DataTransferJobHistory result = dataTransferJobHistoryService.save(dataTransferJobHistory);
        return ResponseEntity.created(new URI("/api/data-transfer-job-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /data-transfer-job-histories : Updates an existing dataTransferJobHistory.
     *
     * @param dataTransferJobHistory the dataTransferJobHistory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dataTransferJobHistory,
     * or with status 400 (Bad Request) if the dataTransferJobHistory is not valid,
     * or with status 500 (Internal Server Error) if the dataTransferJobHistory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/data-transfer-job-histories")
    @Timed
    public ResponseEntity<DataTransferJobHistory> updateDataTransferJobHistory(@Valid @RequestBody DataTransferJobHistory dataTransferJobHistory) throws URISyntaxException {
        log.debug("REST request to update DataTransferJobHistory : {}", dataTransferJobHistory);
        if (dataTransferJobHistory.getId() == null) {
            return createDataTransferJobHistory(dataTransferJobHistory);
        }
        DataTransferJobHistory result = dataTransferJobHistoryService.save(dataTransferJobHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dataTransferJobHistory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /data-transfer-job-histories : get all the dataTransferJobHistories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dataTransferJobHistories in body
     */
    @GetMapping("/data-transfer-job-histories")
    @Timed
    public List<DataTransferJobHistory> getAllDataTransferJobHistories() {
        log.debug("REST request to get all DataTransferJobHistories");
        return dataTransferJobHistoryService.findAll();
        }

    /**
     * GET  /data-transfer-job-histories/:id : get the "id" dataTransferJobHistory.
     *
     * @param id the id of the dataTransferJobHistory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dataTransferJobHistory, or with status 404 (Not Found)
     */
    @GetMapping("/data-transfer-job-histories/{id}")
    @Timed
    public ResponseEntity<DataTransferJobHistory> getDataTransferJobHistory(@PathVariable Long id) {
        log.debug("REST request to get DataTransferJobHistory : {}", id);
        DataTransferJobHistory dataTransferJobHistory = dataTransferJobHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataTransferJobHistory));
    }

    /**
     * DELETE  /data-transfer-job-histories/:id : delete the "id" dataTransferJobHistory.
     *
     * @param id the id of the dataTransferJobHistory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/data-transfer-job-histories/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataTransferJobHistory(@PathVariable Long id) {
        log.debug("REST request to delete DataTransferJobHistory : {}", id);
        dataTransferJobHistoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
