package ir.rcdat.df.web.rest;

import com.codahale.metrics.annotation.Timed;
import ir.rcdat.df.domain.DataTransferConfig;
import ir.rcdat.df.service.DataTransferConfigService;
import ir.rcdat.df.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DataTransferConfig.
 */
@RestController
@RequestMapping("/api")
public class DataTransferConfigResource {

    private final Logger log = LoggerFactory.getLogger(DataTransferConfigResource.class);

    private static final String ENTITY_NAME = "dataTransferConfig";

    private final DataTransferConfigService dataTransferConfigService;

    public DataTransferConfigResource(DataTransferConfigService dataTransferConfigService) {
        this.dataTransferConfigService = dataTransferConfigService;
    }

    /**
     * POST  /data-transfer-configs : Create a new dataTransferConfig.
     *
     * @param dataTransferConfig the dataTransferConfig to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dataTransferConfig, or with status 400 (Bad Request) if the dataTransferConfig has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/data-transfer-configs")
    @Timed
    public ResponseEntity<DataTransferConfig> createDataTransferConfig(@Valid @RequestBody DataTransferConfig dataTransferConfig) throws URISyntaxException {
        log.debug("REST request to save DataTransferConfig : {}", dataTransferConfig);
        if (dataTransferConfig.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dataTransferConfig cannot already have an ID")).body(null);
        }
        DataTransferConfig result = dataTransferConfigService.save(dataTransferConfig);
        return ResponseEntity.created(new URI("/api/data-transfer-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /data-transfer-configs : Updates an existing dataTransferConfig.
     *
     * @param dataTransferConfig the dataTransferConfig to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dataTransferConfig,
     * or with status 400 (Bad Request) if the dataTransferConfig is not valid,
     * or with status 500 (Internal Server Error) if the dataTransferConfig couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/data-transfer-configs")
    @Timed
    public ResponseEntity<DataTransferConfig> updateDataTransferConfig(@Valid @RequestBody DataTransferConfig dataTransferConfig) throws URISyntaxException {
        log.debug("REST request to update DataTransferConfig : {}", dataTransferConfig);
        if (dataTransferConfig.getId() == null) {
            return createDataTransferConfig(dataTransferConfig);
        }
        DataTransferConfig result = dataTransferConfigService.save(dataTransferConfig);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dataTransferConfig.getId().toString()))
            .body(result);
    }

    /**
     * GET  /data-transfer-configs : get all the dataTransferConfigs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dataTransferConfigs in body
     */
    @GetMapping("/data-transfer-configs")
    @Timed
    public List<DataTransferConfig> getAllDataTransferConfigs() {
        log.debug("REST request to get all DataTransferConfigs");
        return dataTransferConfigService.findAll();
        }

    /**
     * GET  /data-transfer-configs/:id : get the "id" dataTransferConfig.
     *
     * @param id the id of the dataTransferConfig to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dataTransferConfig, or with status 404 (Not Found)
     */
    @GetMapping("/data-transfer-configs/{id}")
    @Timed
    public ResponseEntity<DataTransferConfig> getDataTransferConfig(@PathVariable Long id) {
        log.debug("REST request to get DataTransferConfig : {}", id);
        DataTransferConfig dataTransferConfig = dataTransferConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dataTransferConfig));
    }

    /**
     * DELETE  /data-transfer-configs/:id : delete the "id" dataTransferConfig.
     *
     * @param id the id of the dataTransferConfig to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/data-transfer-configs/{id}")
    @Timed
    public ResponseEntity<Void> deleteDataTransferConfig(@PathVariable Long id) {
        log.debug("REST request to delete DataTransferConfig : {}", id);
        dataTransferConfigService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
