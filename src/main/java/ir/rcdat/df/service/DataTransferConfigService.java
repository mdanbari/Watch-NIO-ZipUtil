package ir.rcdat.df.service;

import ir.rcdat.df.domain.DataTransferConfig;
import java.util.List;

/**
 * Service Interface for managing DataTransferConfig.
 */
public interface DataTransferConfigService {

    /**
     * Save a dataTransferConfig.
     *
     * @param dataTransferConfig the entity to save
     * @return the persisted entity
     */
    DataTransferConfig save(DataTransferConfig dataTransferConfig);

    /**
     *  Get all the dataTransferConfigs.
     *
     *  @return the list of entities
     */
    List<DataTransferConfig> findAll();

    /**
     *  Get the "id" dataTransferConfig.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    DataTransferConfig findOne(Long id);

    /**
     *  Delete the "id" dataTransferConfig.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
