package ir.rcdat.df.service.impl;

import ir.rcdat.df.service.DataTransferJobHistoryService;
import ir.rcdat.df.domain.DataTransferJobHistory;
import ir.rcdat.df.repository.DataTransferJobHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing DataTransferJobHistory.
 */
@Service
@Transactional
public class DataTransferJobHistoryServiceImpl implements DataTransferJobHistoryService{

    private final Logger log = LoggerFactory.getLogger(DataTransferJobHistoryServiceImpl.class);

    private final DataTransferJobHistoryRepository dataTransferJobHistoryRepository;

    public DataTransferJobHistoryServiceImpl(DataTransferJobHistoryRepository dataTransferJobHistoryRepository) {
        this.dataTransferJobHistoryRepository = dataTransferJobHistoryRepository;
    }

    /**
     * Save a dataTransferJobHistory.
     *
     * @param dataTransferJobHistory the entity to save
     * @return the persisted entity
     */
    @Override
    public DataTransferJobHistory save(DataTransferJobHistory dataTransferJobHistory) {
        log.debug("Request to save DataTransferJobHistory : {}", dataTransferJobHistory);
        return dataTransferJobHistoryRepository.save(dataTransferJobHistory);
    }

    /**
     *  Get all the dataTransferJobHistories.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<DataTransferJobHistory> findAll() {
        log.debug("Request to get all DataTransferJobHistories");
        return dataTransferJobHistoryRepository.findAll();
    }

    /**
     *  Get one dataTransferJobHistory by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DataTransferJobHistory findOne(Long id) {
        log.debug("Request to get DataTransferJobHistory : {}", id);
        return dataTransferJobHistoryRepository.findOne(id);
    }

    /**
     *  Delete the  dataTransferJobHistory by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DataTransferJobHistory : {}", id);
        dataTransferJobHistoryRepository.delete(id);
    }
}
