package ir.rcdat.df.service.impl;

import ir.rcdat.df.service.DataTransferConfigService;
import ir.rcdat.df.domain.DataTransferConfig;
import ir.rcdat.df.repository.DataTransferConfigRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing DataTransferConfig.
 */
@Service
@Transactional
public class DataTransferConfigServiceImpl implements DataTransferConfigService{

    private final Logger log = LoggerFactory.getLogger(DataTransferConfigServiceImpl.class);

    private final DataTransferConfigRepository dataTransferConfigRepository;

    public DataTransferConfigServiceImpl(DataTransferConfigRepository dataTransferConfigRepository) {
        this.dataTransferConfigRepository = dataTransferConfigRepository;
    }

    /**
     * Save a dataTransferConfig.
     *
     * @param dataTransferConfig the entity to save
     * @return the persisted entity
     */
    @Override
    public DataTransferConfig save(DataTransferConfig dataTransferConfig) {
        log.debug("Request to save DataTransferConfig : {}", dataTransferConfig);
        return dataTransferConfigRepository.save(dataTransferConfig);
    }

    /**
     *  Get all the dataTransferConfigs.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<DataTransferConfig> findAll() {
        log.debug("Request to get all DataTransferConfigs");
        return dataTransferConfigRepository.findAll();
    }

    /**
     *  Get one dataTransferConfig by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public DataTransferConfig findOne(Long id) {
        log.debug("Request to get DataTransferConfig : {}", id);
        return dataTransferConfigRepository.findOne(id);
    }

    /**
     *  Delete the  dataTransferConfig by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DataTransferConfig : {}", id);
        dataTransferConfigRepository.delete(id);
    }
}
