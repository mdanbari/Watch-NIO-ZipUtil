package ir.rcdat.df.service;

import ir.rcdat.df.domain.DataTransferJobHistory;
import java.util.List;

/**
 * Service Interface for managing DataTransferJobHistory.
 */
public interface DataTransferJobHistoryService {

    /**
     * Save a dataTransferJobHistory.
     *
     * @param dataTransferJobHistory the entity to save
     * @return the persisted entity
     */
    DataTransferJobHistory save(DataTransferJobHistory dataTransferJobHistory);

    /**
     *  Get all the dataTransferJobHistories.
     *
     *  @return the list of entities
     */
    List<DataTransferJobHistory> findAll();

    /**
     *  Get the "id" dataTransferJobHistory.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    DataTransferJobHistory findOne(Long id);

    /**
     *  Delete the "id" dataTransferJobHistory.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
