package ir.rcdat.df.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import ir.rcdat.df.domain.enumeration.RemoteType;

/**
 * A DataTransferConfig.
 */
@Entity
@Table(name = "data_transfer_config")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DataTransferConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "source_folder_path", nullable = false)
    private String sourceFolderPath;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "remote_type", nullable = false)
    private RemoteType remoteType;

    @NotNull
    @Column(name = "remote_address", nullable = false)
    private String remoteAddress;

    @Column(name = "remote_username")
    private String remoteUsername;

    @Column(name = "remote_password")
    private String remotePassword;

    @Column(name = "job_intervalsec")
    private Integer jobIntervalsec;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSourceFolderPath() {
        return sourceFolderPath;
    }

    public DataTransferConfig sourceFolderPath(String sourceFolderPath) {
        this.sourceFolderPath = sourceFolderPath;
        return this;
    }

    public void setSourceFolderPath(String sourceFolderPath) {
        this.sourceFolderPath = sourceFolderPath;
    }

    public RemoteType getRemoteType() {
        return remoteType;
    }

    public DataTransferConfig remoteType(RemoteType remoteType) {
        this.remoteType = remoteType;
        return this;
    }

    public void setRemoteType(RemoteType remoteType) {
        this.remoteType = remoteType;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public DataTransferConfig remoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
        return this;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public String getRemoteUsername() {
        return remoteUsername;
    }

    public DataTransferConfig remoteUsername(String remoteUsername) {
        this.remoteUsername = remoteUsername;
        return this;
    }

    public void setRemoteUsername(String remoteUsername) {
        this.remoteUsername = remoteUsername;
    }

    public String getRemotePassword() {
        return remotePassword;
    }

    public DataTransferConfig remotePassword(String remotePassword) {
        this.remotePassword = remotePassword;
        return this;
    }

    public void setRemotePassword(String remotePassword) {
        this.remotePassword = remotePassword;
    }

    public Integer getJobIntervalsec() {
        return jobIntervalsec;
    }

    public DataTransferConfig jobIntervalsec(Integer jobIntervalsec) {
        this.jobIntervalsec = jobIntervalsec;
        return this;
    }

    public void setJobIntervalsec(Integer jobIntervalsec) {
        this.jobIntervalsec = jobIntervalsec;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataTransferConfig dataTransferConfig = (DataTransferConfig) o;
        if (dataTransferConfig.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dataTransferConfig.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DataTransferConfig{" +
            "id=" + getId() +
            ", sourceFolderPath='" + getSourceFolderPath() + "'" +
            ", remoteType='" + getRemoteType() + "'" +
            ", remoteAddress='" + getRemoteAddress() + "'" +
            ", remoteUsername='" + getRemoteUsername() + "'" +
            ", remotePassword='" + getRemotePassword() + "'" +
            ", jobIntervalsec='" + getJobIntervalsec() + "'" +
            "}";
    }
}
