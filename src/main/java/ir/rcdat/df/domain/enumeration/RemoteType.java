package ir.rcdat.df.domain.enumeration;

/**
 * The RemoteType enumeration.
 */
public enum RemoteType {
    FTP, SMB
}
