package ir.rcdat.df.repository;

import ir.rcdat.df.domain.DataTransferJobHistory;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DataTransferJobHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataTransferJobHistoryRepository extends JpaRepository<DataTransferJobHistory, Long> {

}
