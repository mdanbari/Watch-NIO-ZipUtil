package ir.rcdat.df.repository;

import ir.rcdat.df.domain.DataTransferConfig;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the DataTransferConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DataTransferConfigRepository extends JpaRepository<DataTransferConfig, Long> {

}
