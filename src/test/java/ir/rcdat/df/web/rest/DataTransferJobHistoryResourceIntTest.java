package ir.rcdat.df.web.rest;

import ir.rcdat.df.DatatransferApp;

import ir.rcdat.df.domain.DataTransferJobHistory;
import ir.rcdat.df.repository.DataTransferJobHistoryRepository;
import ir.rcdat.df.service.DataTransferJobHistoryService;
import ir.rcdat.df.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static ir.rcdat.df.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DataTransferJobHistoryResource REST controller.
 *
 * @see DataTransferJobHistoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatatransferApp.class)
public class DataTransferJobHistoryResourceIntTest {

    private static final ZonedDateTime DEFAULT_EXECUTION_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_EXECUTION_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBBBBBBB";

    @Autowired
    private DataTransferJobHistoryRepository dataTransferJobHistoryRepository;

    @Autowired
    private DataTransferJobHistoryService dataTransferJobHistoryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDataTransferJobHistoryMockMvc;

    private DataTransferJobHistory dataTransferJobHistory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DataTransferJobHistoryResource dataTransferJobHistoryResource = new DataTransferJobHistoryResource(dataTransferJobHistoryService);
        this.restDataTransferJobHistoryMockMvc = MockMvcBuilders.standaloneSetup(dataTransferJobHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataTransferJobHistory createEntity(EntityManager em) {
        DataTransferJobHistory dataTransferJobHistory = new DataTransferJobHistory()
            .executionTime(DEFAULT_EXECUTION_TIME)
            .fileName(DEFAULT_FILE_NAME);
        return dataTransferJobHistory;
    }

    @Before
    public void initTest() {
        dataTransferJobHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createDataTransferJobHistory() throws Exception {
        int databaseSizeBeforeCreate = dataTransferJobHistoryRepository.findAll().size();

        // Create the DataTransferJobHistory
        restDataTransferJobHistoryMockMvc.perform(post("/api/data-transfer-job-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataTransferJobHistory)))
            .andExpect(status().isCreated());

        // Validate the DataTransferJobHistory in the database
        List<DataTransferJobHistory> dataTransferJobHistoryList = dataTransferJobHistoryRepository.findAll();
        assertThat(dataTransferJobHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        DataTransferJobHistory testDataTransferJobHistory = dataTransferJobHistoryList.get(dataTransferJobHistoryList.size() - 1);
        assertThat(testDataTransferJobHistory.getExecutionTime()).isEqualTo(DEFAULT_EXECUTION_TIME);
        assertThat(testDataTransferJobHistory.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
    }

    @Test
    @Transactional
    public void createDataTransferJobHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dataTransferJobHistoryRepository.findAll().size();

        // Create the DataTransferJobHistory with an existing ID
        dataTransferJobHistory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataTransferJobHistoryMockMvc.perform(post("/api/data-transfer-job-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataTransferJobHistory)))
            .andExpect(status().isBadRequest());

        // Validate the DataTransferJobHistory in the database
        List<DataTransferJobHistory> dataTransferJobHistoryList = dataTransferJobHistoryRepository.findAll();
        assertThat(dataTransferJobHistoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkExecutionTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataTransferJobHistoryRepository.findAll().size();
        // set the field null
        dataTransferJobHistory.setExecutionTime(null);

        // Create the DataTransferJobHistory, which fails.

        restDataTransferJobHistoryMockMvc.perform(post("/api/data-transfer-job-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataTransferJobHistory)))
            .andExpect(status().isBadRequest());

        List<DataTransferJobHistory> dataTransferJobHistoryList = dataTransferJobHistoryRepository.findAll();
        assertThat(dataTransferJobHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDataTransferJobHistories() throws Exception {
        // Initialize the database
        dataTransferJobHistoryRepository.saveAndFlush(dataTransferJobHistory);

        // Get all the dataTransferJobHistoryList
        restDataTransferJobHistoryMockMvc.perform(get("/api/data-transfer-job-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataTransferJobHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].executionTime").value(hasItem(sameInstant(DEFAULT_EXECUTION_TIME))))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME.toString())));
    }

    @Test
    @Transactional
    public void getDataTransferJobHistory() throws Exception {
        // Initialize the database
        dataTransferJobHistoryRepository.saveAndFlush(dataTransferJobHistory);

        // Get the dataTransferJobHistory
        restDataTransferJobHistoryMockMvc.perform(get("/api/data-transfer-job-histories/{id}", dataTransferJobHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dataTransferJobHistory.getId().intValue()))
            .andExpect(jsonPath("$.executionTime").value(sameInstant(DEFAULT_EXECUTION_TIME)))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDataTransferJobHistory() throws Exception {
        // Get the dataTransferJobHistory
        restDataTransferJobHistoryMockMvc.perform(get("/api/data-transfer-job-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDataTransferJobHistory() throws Exception {
        // Initialize the database
        dataTransferJobHistoryService.save(dataTransferJobHistory);

        int databaseSizeBeforeUpdate = dataTransferJobHistoryRepository.findAll().size();

        // Update the dataTransferJobHistory
        DataTransferJobHistory updatedDataTransferJobHistory = dataTransferJobHistoryRepository.findOne(dataTransferJobHistory.getId());
        updatedDataTransferJobHistory
            .executionTime(UPDATED_EXECUTION_TIME)
            .fileName(UPDATED_FILE_NAME);

        restDataTransferJobHistoryMockMvc.perform(put("/api/data-transfer-job-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDataTransferJobHistory)))
            .andExpect(status().isOk());

        // Validate the DataTransferJobHistory in the database
        List<DataTransferJobHistory> dataTransferJobHistoryList = dataTransferJobHistoryRepository.findAll();
        assertThat(dataTransferJobHistoryList).hasSize(databaseSizeBeforeUpdate);
        DataTransferJobHistory testDataTransferJobHistory = dataTransferJobHistoryList.get(dataTransferJobHistoryList.size() - 1);
        assertThat(testDataTransferJobHistory.getExecutionTime()).isEqualTo(UPDATED_EXECUTION_TIME);
        assertThat(testDataTransferJobHistory.getFileName()).isEqualTo(UPDATED_FILE_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingDataTransferJobHistory() throws Exception {
        int databaseSizeBeforeUpdate = dataTransferJobHistoryRepository.findAll().size();

        // Create the DataTransferJobHistory

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDataTransferJobHistoryMockMvc.perform(put("/api/data-transfer-job-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataTransferJobHistory)))
            .andExpect(status().isCreated());

        // Validate the DataTransferJobHistory in the database
        List<DataTransferJobHistory> dataTransferJobHistoryList = dataTransferJobHistoryRepository.findAll();
        assertThat(dataTransferJobHistoryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDataTransferJobHistory() throws Exception {
        // Initialize the database
        dataTransferJobHistoryService.save(dataTransferJobHistory);

        int databaseSizeBeforeDelete = dataTransferJobHistoryRepository.findAll().size();

        // Get the dataTransferJobHistory
        restDataTransferJobHistoryMockMvc.perform(delete("/api/data-transfer-job-histories/{id}", dataTransferJobHistory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DataTransferJobHistory> dataTransferJobHistoryList = dataTransferJobHistoryRepository.findAll();
        assertThat(dataTransferJobHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataTransferJobHistory.class);
        DataTransferJobHistory dataTransferJobHistory1 = new DataTransferJobHistory();
        dataTransferJobHistory1.setId(1L);
        DataTransferJobHistory dataTransferJobHistory2 = new DataTransferJobHistory();
        dataTransferJobHistory2.setId(dataTransferJobHistory1.getId());
        assertThat(dataTransferJobHistory1).isEqualTo(dataTransferJobHistory2);
        dataTransferJobHistory2.setId(2L);
        assertThat(dataTransferJobHistory1).isNotEqualTo(dataTransferJobHistory2);
        dataTransferJobHistory1.setId(null);
        assertThat(dataTransferJobHistory1).isNotEqualTo(dataTransferJobHistory2);
    }
}
