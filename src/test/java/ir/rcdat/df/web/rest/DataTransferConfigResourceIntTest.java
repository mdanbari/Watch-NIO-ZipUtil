package ir.rcdat.df.web.rest;

import ir.rcdat.df.DatatransferApp;

import ir.rcdat.df.domain.DataTransferConfig;
import ir.rcdat.df.repository.DataTransferConfigRepository;
import ir.rcdat.df.service.DataTransferConfigService;
import ir.rcdat.df.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ir.rcdat.df.domain.enumeration.RemoteType;
/**
 * Test class for the DataTransferConfigResource REST controller.
 *
 * @see DataTransferConfigResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatatransferApp.class)
public class DataTransferConfigResourceIntTest {

    private static final String DEFAULT_SOURCE_FOLDER_PATH = "AAAAAAAAAA";
    private static final String UPDATED_SOURCE_FOLDER_PATH = "BBBBBBBBBB";

    private static final RemoteType DEFAULT_REMOTE_TYPE = RemoteType.FTP;
    private static final RemoteType UPDATED_REMOTE_TYPE = RemoteType.SMB;

    private static final String DEFAULT_REMOTE_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_REMOTE_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_REMOTE_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_REMOTE_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_REMOTE_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_REMOTE_PASSWORD = "BBBBBBBBBB";

    private static final Integer DEFAULT_JOB_INTERVALSEC = 1;
    private static final Integer UPDATED_JOB_INTERVALSEC = 2;

    @Autowired
    private DataTransferConfigRepository dataTransferConfigRepository;

    @Autowired
    private DataTransferConfigService dataTransferConfigService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDataTransferConfigMockMvc;

    private DataTransferConfig dataTransferConfig;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DataTransferConfigResource dataTransferConfigResource = new DataTransferConfigResource(dataTransferConfigService);
        this.restDataTransferConfigMockMvc = MockMvcBuilders.standaloneSetup(dataTransferConfigResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DataTransferConfig createEntity(EntityManager em) {
        DataTransferConfig dataTransferConfig = new DataTransferConfig()
            .sourceFolderPath(DEFAULT_SOURCE_FOLDER_PATH)
            .remoteType(DEFAULT_REMOTE_TYPE)
            .remoteAddress(DEFAULT_REMOTE_ADDRESS)
            .remoteUsername(DEFAULT_REMOTE_USERNAME)
            .remotePassword(DEFAULT_REMOTE_PASSWORD)
            .jobIntervalsec(DEFAULT_JOB_INTERVALSEC);
        return dataTransferConfig;
    }

    @Before
    public void initTest() {
        dataTransferConfig = createEntity(em);
    }

    @Test
    @Transactional
    public void createDataTransferConfig() throws Exception {
        int databaseSizeBeforeCreate = dataTransferConfigRepository.findAll().size();

        // Create the DataTransferConfig
        restDataTransferConfigMockMvc.perform(post("/api/data-transfer-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataTransferConfig)))
            .andExpect(status().isCreated());

        // Validate the DataTransferConfig in the database
        List<DataTransferConfig> dataTransferConfigList = dataTransferConfigRepository.findAll();
        assertThat(dataTransferConfigList).hasSize(databaseSizeBeforeCreate + 1);
        DataTransferConfig testDataTransferConfig = dataTransferConfigList.get(dataTransferConfigList.size() - 1);
        assertThat(testDataTransferConfig.getSourceFolderPath()).isEqualTo(DEFAULT_SOURCE_FOLDER_PATH);
        assertThat(testDataTransferConfig.getRemoteType()).isEqualTo(DEFAULT_REMOTE_TYPE);
        assertThat(testDataTransferConfig.getRemoteAddress()).isEqualTo(DEFAULT_REMOTE_ADDRESS);
        assertThat(testDataTransferConfig.getRemoteUsername()).isEqualTo(DEFAULT_REMOTE_USERNAME);
        assertThat(testDataTransferConfig.getRemotePassword()).isEqualTo(DEFAULT_REMOTE_PASSWORD);
        assertThat(testDataTransferConfig.getJobIntervalsec()).isEqualTo(DEFAULT_JOB_INTERVALSEC);
    }

    @Test
    @Transactional
    public void createDataTransferConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dataTransferConfigRepository.findAll().size();

        // Create the DataTransferConfig with an existing ID
        dataTransferConfig.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDataTransferConfigMockMvc.perform(post("/api/data-transfer-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataTransferConfig)))
            .andExpect(status().isBadRequest());

        // Validate the DataTransferConfig in the database
        List<DataTransferConfig> dataTransferConfigList = dataTransferConfigRepository.findAll();
        assertThat(dataTransferConfigList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkSourceFolderPathIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataTransferConfigRepository.findAll().size();
        // set the field null
        dataTransferConfig.setSourceFolderPath(null);

        // Create the DataTransferConfig, which fails.

        restDataTransferConfigMockMvc.perform(post("/api/data-transfer-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataTransferConfig)))
            .andExpect(status().isBadRequest());

        List<DataTransferConfig> dataTransferConfigList = dataTransferConfigRepository.findAll();
        assertThat(dataTransferConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRemoteTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataTransferConfigRepository.findAll().size();
        // set the field null
        dataTransferConfig.setRemoteType(null);

        // Create the DataTransferConfig, which fails.

        restDataTransferConfigMockMvc.perform(post("/api/data-transfer-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataTransferConfig)))
            .andExpect(status().isBadRequest());

        List<DataTransferConfig> dataTransferConfigList = dataTransferConfigRepository.findAll();
        assertThat(dataTransferConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRemoteAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = dataTransferConfigRepository.findAll().size();
        // set the field null
        dataTransferConfig.setRemoteAddress(null);

        // Create the DataTransferConfig, which fails.

        restDataTransferConfigMockMvc.perform(post("/api/data-transfer-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataTransferConfig)))
            .andExpect(status().isBadRequest());

        List<DataTransferConfig> dataTransferConfigList = dataTransferConfigRepository.findAll();
        assertThat(dataTransferConfigList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDataTransferConfigs() throws Exception {
        // Initialize the database
        dataTransferConfigRepository.saveAndFlush(dataTransferConfig);

        // Get all the dataTransferConfigList
        restDataTransferConfigMockMvc.perform(get("/api/data-transfer-configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dataTransferConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].sourceFolderPath").value(hasItem(DEFAULT_SOURCE_FOLDER_PATH.toString())))
            .andExpect(jsonPath("$.[*].remoteType").value(hasItem(DEFAULT_REMOTE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].remoteAddress").value(hasItem(DEFAULT_REMOTE_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].remoteUsername").value(hasItem(DEFAULT_REMOTE_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].remotePassword").value(hasItem(DEFAULT_REMOTE_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].jobIntervalsec").value(hasItem(DEFAULT_JOB_INTERVALSEC)));
    }

    @Test
    @Transactional
    public void getDataTransferConfig() throws Exception {
        // Initialize the database
        dataTransferConfigRepository.saveAndFlush(dataTransferConfig);

        // Get the dataTransferConfig
        restDataTransferConfigMockMvc.perform(get("/api/data-transfer-configs/{id}", dataTransferConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dataTransferConfig.getId().intValue()))
            .andExpect(jsonPath("$.sourceFolderPath").value(DEFAULT_SOURCE_FOLDER_PATH.toString()))
            .andExpect(jsonPath("$.remoteType").value(DEFAULT_REMOTE_TYPE.toString()))
            .andExpect(jsonPath("$.remoteAddress").value(DEFAULT_REMOTE_ADDRESS.toString()))
            .andExpect(jsonPath("$.remoteUsername").value(DEFAULT_REMOTE_USERNAME.toString()))
            .andExpect(jsonPath("$.remotePassword").value(DEFAULT_REMOTE_PASSWORD.toString()))
            .andExpect(jsonPath("$.jobIntervalsec").value(DEFAULT_JOB_INTERVALSEC));
    }

    @Test
    @Transactional
    public void getNonExistingDataTransferConfig() throws Exception {
        // Get the dataTransferConfig
        restDataTransferConfigMockMvc.perform(get("/api/data-transfer-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDataTransferConfig() throws Exception {
        // Initialize the database
        dataTransferConfigService.save(dataTransferConfig);

        int databaseSizeBeforeUpdate = dataTransferConfigRepository.findAll().size();

        // Update the dataTransferConfig
        DataTransferConfig updatedDataTransferConfig = dataTransferConfigRepository.findOne(dataTransferConfig.getId());
        updatedDataTransferConfig
            .sourceFolderPath(UPDATED_SOURCE_FOLDER_PATH)
            .remoteType(UPDATED_REMOTE_TYPE)
            .remoteAddress(UPDATED_REMOTE_ADDRESS)
            .remoteUsername(UPDATED_REMOTE_USERNAME)
            .remotePassword(UPDATED_REMOTE_PASSWORD)
            .jobIntervalsec(UPDATED_JOB_INTERVALSEC);

        restDataTransferConfigMockMvc.perform(put("/api/data-transfer-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDataTransferConfig)))
            .andExpect(status().isOk());

        // Validate the DataTransferConfig in the database
        List<DataTransferConfig> dataTransferConfigList = dataTransferConfigRepository.findAll();
        assertThat(dataTransferConfigList).hasSize(databaseSizeBeforeUpdate);
        DataTransferConfig testDataTransferConfig = dataTransferConfigList.get(dataTransferConfigList.size() - 1);
        assertThat(testDataTransferConfig.getSourceFolderPath()).isEqualTo(UPDATED_SOURCE_FOLDER_PATH);
        assertThat(testDataTransferConfig.getRemoteType()).isEqualTo(UPDATED_REMOTE_TYPE);
        assertThat(testDataTransferConfig.getRemoteAddress()).isEqualTo(UPDATED_REMOTE_ADDRESS);
        assertThat(testDataTransferConfig.getRemoteUsername()).isEqualTo(UPDATED_REMOTE_USERNAME);
        assertThat(testDataTransferConfig.getRemotePassword()).isEqualTo(UPDATED_REMOTE_PASSWORD);
        assertThat(testDataTransferConfig.getJobIntervalsec()).isEqualTo(UPDATED_JOB_INTERVALSEC);
    }

    @Test
    @Transactional
    public void updateNonExistingDataTransferConfig() throws Exception {
        int databaseSizeBeforeUpdate = dataTransferConfigRepository.findAll().size();

        // Create the DataTransferConfig

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDataTransferConfigMockMvc.perform(put("/api/data-transfer-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dataTransferConfig)))
            .andExpect(status().isCreated());

        // Validate the DataTransferConfig in the database
        List<DataTransferConfig> dataTransferConfigList = dataTransferConfigRepository.findAll();
        assertThat(dataTransferConfigList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDataTransferConfig() throws Exception {
        // Initialize the database
        dataTransferConfigService.save(dataTransferConfig);

        int databaseSizeBeforeDelete = dataTransferConfigRepository.findAll().size();

        // Get the dataTransferConfig
        restDataTransferConfigMockMvc.perform(delete("/api/data-transfer-configs/{id}", dataTransferConfig.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DataTransferConfig> dataTransferConfigList = dataTransferConfigRepository.findAll();
        assertThat(dataTransferConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DataTransferConfig.class);
        DataTransferConfig dataTransferConfig1 = new DataTransferConfig();
        dataTransferConfig1.setId(1L);
        DataTransferConfig dataTransferConfig2 = new DataTransferConfig();
        dataTransferConfig2.setId(dataTransferConfig1.getId());
        assertThat(dataTransferConfig1).isEqualTo(dataTransferConfig2);
        dataTransferConfig2.setId(2L);
        assertThat(dataTransferConfig1).isNotEqualTo(dataTransferConfig2);
        dataTransferConfig1.setId(null);
        assertThat(dataTransferConfig1).isNotEqualTo(dataTransferConfig2);
    }
}
